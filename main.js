let state = {
    searchInputData: null,
    url: null,
    searchCategory: null,
    value: null,
    data: null
    /*drinks:{
    idDrink: null,
    strDrink:null,
    strDrinkThumb: null,
    strCategory: null, 
    strGlass: null
    }*/
    }
    
    //mostrar secciones de la pantalla
    let initialSection = document.getElementById("InitialState");
    function showInitialSection() {
    initialSection.style.display = "block";
    searchSection.style.display = "none";
    inputDataSection.style.display = "none";
    resultSection.style.display = "none";
    }
    
    let searchSection = document.getElementById("CategoryState");
    function showSearchSection() {
    initialSection.style.display = "none";
    searchSection.style.display = "block";
    inputDataSection.style.display = "none";
    resultSection.style.display = "none";
    }
    
    let inputDataSection = document.getElementById("EnterDataState");
    function showSectionEnterData() {
    initialSection.style.display = "none";
    searchSection.style.display = "none";
    inputDataSection.style.display = "block";
    resultSection.style.display = "none";
    }
    
    let resultSection = document.getElementById("PrintResultState");
    function showResultSection() {
    initialSection.style.display = "none";
    searchSection.style.display = "none";
    inputDataSection.style.display = "none";
    resultSection.style.display = "block";
    }
    
    //declararacion de los botones
    
    let startButton=document.getElementById("StartButton");
    startButton.addEventListener("click", showSearchSection);
    
    let backButtonCategory=document.getElementById("BackButtonCategory");
    backButtonCategory.addEventListener("click", showInitialSection);
    
    let searchButton=document.getElementById("SearchButtonData");
    searchButton.addEventListener("click", getInfoSearchName);
    
    
    let backButtonData=document.getElementById("BackButtonData");
    backButtonData.addEventListener("click", showSearchSection );
    
    let exitButtonData=document.getElementById("ExitButtonData");
    exitButtonData.addEventListener("click", showInitialSection );
    
    let exitButtonResult=document.getElementById("ExitButtonResult");
    exitButtonResult.addEventListener("click", showInitialSection);
    
    let searchByName=document.getElementById("ButtonName");
    let searchByIngredient=document.getElementById("ButtonIngredient");
    let searchByNotAlcohol=document.getElementById("ButtonNotAlcohol");
    let searchBySuggestion=document.getElementById("ButtonSuggestion");
    searchByName.addEventListener("click", categoryName);
    searchByIngredient.addEventListener("click",categoryIngredient );
    searchByNotAlcohol.addEventListener("click",categoryNotAlcohol );
    searchBySuggestion.addEventListener("click", categorySuggestion);
    showInitialSection()
    //acciones de los botones de busqueda
    function categoryName() {
    state.searchCategory = "name";
    console.log(state.searchCategory)
    showSectionEnterData()
    }
    
    function categoryIngredient() {
    state.searchCategory = "ingredient";
    showSectionEnterData()
    }
    
    async function categoryNotAlcohol() {
    state.searchCategory = "nonAlcohol";
    generateURL(state)
    await callApi(state) 
    showResultSection()
    
    }
    
    async function categorySuggestion() {
    state.searchCategory = "suggestion";
    generateURL(state)
    await callApi(state) 
    showResultSection()
    }
    //funcion para que capture la informacion del input
    function getInfoInput(state){
    console.log("ziortzaG")
    state.searchInputData = document.getElementById("InputEnterData").value;
    console.log(state.searchInputData)
    
    }
    //funcion para meter el evento de la llamada a la api
    async function getInfoSearchName(Event){
    await callApi(state)
    showResultSection()
    
    }
    
    //funcion si ha ingresado un dato en el input
    function ifEnterData(){
    let inputNull = document.querySelector("input[name='InputEnterData']:checked");
    if (inputNull===null){return false}
    else {return true}}
    
    
    
    //funcion generar la url dependiendo de la categoria
    
    function generateURL(state) {
    console.log(state.searchCategory);
    console.log(state.searchInputData+"zior2");
    
    console.log(state.searchCategory === "name")
    
    if (state.searchCategory === "name") {
    getInfoInput(state);
    state.url ="https://www.thecocktaildb.com/api/json/v1/1/search.php?s=" + state.searchInputData;
    console.log(state.searchInputData+"zior3");
    }
    if (state.searchCategory === "ingredient") {
    getInfoInput(state);
    state.url ="https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=" + state.searchInputData;}
    if (state.searchCategory === "nonAlcohol") {
    state.url ="https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Non_Alcoholic";}
    if (state.searchCategory === "suggestion") {
    state.url = "https://www.thecocktaildb.com/api/json/v1/1/random.php";
    }
    }
    
    
    
    
    
    //funcion que llamara a la api
    
    async function callApi(state) {
    console.log("callapi");
    console.log(state.searchCategory);
    //tomarinfoinput(state);//lo coloco zior
    console.log("cesar1")
    generateURL(state); 
    console.log("cesar2");
    console.log(state.searchCategory)
    console.log('url', state.url)
    let res = await fetch(state.url);
    state.data = await res.json();
    console.log("aaaa");
    // printResul(mickeymouse);
    printResul(state)
    }
    
    
    function printResul(state) {
    
    
    console.log(state.searchCategory === "name")
    if (state.searchCategory === "name") { 
    resultByName(state);
    console.log("impremenombre")
    }
    if (state.searchCategory === "ingredient") {
    resultByIngredient(state)
    }
    if (state.searchCategory === "nonAlcohol") {
    resultByIngredient(state)
    }
    if (state.searchCategory === "suggestion") {
    resultByName(state);;
    }
    showResultSection()
    }
    
    function resultByName(state){
    console.log("zior5")
    let result = document.getElementById("PrintResult");
    result.innerHTML = "";
    for (let i = 0; i < state.data.drinks.length; i++) {
        var box = document.createElement("div"); 
        box.setAttribute("id",i);
        box.setAttribute("class","boxRes");
        document.getElementById("PrintResult").append(box);

        var printTextIdDrink = document.createElement("h2"); 
        printTextIdDrink.setAttribute("class","TextIdDrink");
        printTextIdDrink.innerHTML = "<span>Drink Id: </span>";
        document.getElementById(i).append(printTextIdDrink);
  
  
        var printIdDrink = document.createElement("p"); 
        printIdDrink.setAttribute("id","IdDrink");
        printIdDrink.setAttribute("class","IdDrink");
        printIdDrink.innerHTML = state.data.drinks[i].idDrink+" ";
        document.getElementById(i).append(printIdDrink);

        var printTextDrinkName = document.createElement("h2"); 
        printTextDrinkName.setAttribute("class","TextDrinkName");
        printTextDrinkName.innerHTML = "<span>Drink Name: </span>";
        document.getElementById(i).append(printTextDrinkName);
        
        var printStrDrink = document.createElement("p"); 
        printStrDrink.setAttribute("id","StrDrink");
        printStrDrink.setAttribute("class","StrDrink");
        printStrDrink.innerHTML = state.data.drinks[i].strDrink+" ";
        document.getElementById(i).append(printStrDrink);
       
        var printStrDrinkThumb = document.createElement("img"); 
        printStrDrinkThumb.className="DrinkThumb";
        printStrDrinkThumb.src= state.data.drinks[i].strDrinkThumb;
        document.getElementById(i).append(printStrDrinkThumb);

        var printTextCategory = document.createElement("h2"); 
        printTextCategory.setAttribute("class","TextCategory");
        printTextCategory.innerHTML = "<span>Category: </span>";
        document.getElementById(i).append(printTextCategory);
      
        var printStrCategory = document.createElement("p"); 
        printStrCategory.setAttribute("id","Category");
        printStrCategory.setAttribute("class","Category");
        printStrCategory.innerHTML = state.data.drinks[i].strCategory+" ";
        document.getElementById(i).append(printStrCategory);
        
        var printTextTypeGlass = document.createElement("h2"); 
        printTextTypeGlass.setAttribute("class","TextTypeGlass");
        printTextTypeGlass.innerHTML = "<span>Type Glass: </span>";
        document.getElementById(i).append(printTextTypeGlass);

        var printStrGlass = document.createElement("p"); 
        printStrGlass.setAttribute("id","StrGlass");
        printStrGlass.setAttribute("class","StrGlass");
        printStrGlass.innerHTML = state.data.drinks[i].strGlass+" ";
        document.getElementById(i).append(printStrGlass);
  
        var printTextInstructions = document.createElement("h2"); 
        printTextInstructions.setAttribute("class","TextInstructions");
        printTextInstructions.innerHTML = "<span>Instructions: </span>";
        document.getElementById(i).append(printTextInstructions);

        var printStrInstructions = document.createElement("p"); 
        printStrInstructions.setAttribute("id","StrInstructions");
        printStrInstructions.setAttribute("class","StrInstructions");
        printStrInstructions.innerHTML = state.data.drinks[i].strInstructions+" ";
        document.getElementById(i).append(printStrInstructions);

        var printTextIngredient = document.createElement("h2"); 
        printTextIngredient.setAttribute("class","TextIngredient");
        printTextIngredient.innerHTML = "<span>Ingredients: </span>";
        document.getElementById(i).append(printTextIngredient);
        
        var printStrIngredient1 = document.createElement("p"); 
        printStrIngredient1.setAttribute("class","StrIngredient");
        printStrIngredient1.innerHTML = state.data.drinks[i].strIngredient1;
        document.getElementById(i).append(printStrIngredient1);

        var printStrIngredient2 = document.createElement("p"); 
        printStrIngredient2.setAttribute("class","StrIngredient");
        printStrIngredient2.innerHTML = state.data.drinks[i].strIngredient2;
        document.getElementById(i).append(printStrIngredient2);
        
        var printStrIngredient3 = document.createElement("p"); 
        printStrIngredient3.setAttribute("class","StrIngredient");
        printStrIngredient3.innerHTML = state.data.drinks[i].strIngredient3;
        document.getElementById(i).append(printStrIngredient3);
        
        var printStrIngredient4 = document.createElement("p"); 
        printStrIngredient4.setAttribute("class","StrIngredient");
        printStrIngredient4.innerHTML = state.data.drinks[i].strIngredient4;
        document.getElementById(i).append(printStrIngredient4);

        var printStrIngredient5 = document.createElement("p"); 
        printStrIngredient5.setAttribute("class","StrIngredient");
        printStrIngredient5.innerHTML = state.data.drinks[i].strIngredient5;
        document.getElementById(i).append(printStrIngredient5);

        var printStrIngredient6 = document.createElement("p"); 
        printStrIngredient6.setAttribute("class","StrIngredient");
        printStrIngredient6.innerHTML = state.data.drinks[i].strIngredient6;
        document.getElementById(i).append(printStrIngredient6);

        var printStrIngredient7 = document.createElement("p"); 
        printStrIngredient7.setAttribute("class","StrIngredient");
        printStrIngredient7.innerHTML = state.data.drinks[i].strIngredient7;
        document.getElementById(i).append(printStrIngredient7);

        var printStrIngredient8 = document.createElement("p"); 
        printStrIngredient8.setAttribute("class","StrIngredient");
        printStrIngredient8.innerHTML = state.data.drinks[i].strIngredient8;
        document.getElementById(i).append(printStrIngredient8);

        var printStrIngredient9 = document.createElement("p"); 
        printStrIngredient9.setAttribute("class","StrIngredient");
        printStrIngredient9.innerHTML = state.data.drinks[i].strIngredient9;
        document.getElementById(i).append(printStrIngredient9);

        var printStrIngredient10 = document.createElement("p"); 
        printStrIngredient10.setAttribute("class","StrIngredient");
        printStrIngredient10.innerHTML = state.data.drinks[i].strIngredient10;
        document.getElementById(i).append(printStrIngredient10);

        var printStrIngredient11 = document.createElement("p"); 
        printStrIngredient11.setAttribute("class","StrIngredient");
        printStrIngredient11.innerHTML = state.data.drinks[i].strIngredient11;
        document.getElementById(i).append(printStrIngredient11);

        var printStrIngredient12 = document.createElement("p"); 
        printStrIngredient12.setAttribute("class","StrIngredient");
        printStrIngredient12.innerHTML = state.data.drinks[i].strIngredient12;
        document.getElementById(i).append(printStrIngredient12);

        var printStrIngredient13 = document.createElement("p"); 
        printStrIngredient13.setAttribute("class","StrIngredient");
        printStrIngredient13.innerHTML = state.data.drinks[i].strIngredient13;
        document.getElementById(i).append(printStrIngredient13);

        var printStrIngredient14 = document.createElement("p"); 
        printStrIngredient14.setAttribute("class","StrIngredient");
        printStrIngredient14.innerHTML = state.data.drinks[i].strIngredient14;
        document.getElementById(i).append(printStrIngredient14);

        var printStrIngredient15 = document.createElement("p"); 
        printStrIngredient15.setAttribute("class","StrIngredient");
        printStrIngredient15.innerHTML = state.data.drinks[i].strIngredient15;
        document.getElementById(i).append(printStrIngredient15);
   
    
    } }
    
   
    
    
    function resultByIngredient(state){
    console.log("zior5")
    let result = document.getElementById("PrintResult");
    result.innerHTML = "";
    for (let i = 0; i < state.data.drinks.length; i++) {
        console.log("zior"+i);
        var box = document.createElement("div"); 
        box.setAttribute("id",i);
        box.setAttribute("class","boxRes");
        document.getElementById("PrintResult").append(box);
  
        var printIdDrink = document.createElement("p"); 
        printIdDrink.setAttribute("id","IdDrink");
        printIdDrink.setAttribute("class","IdDrink");
        printIdDrink.innerHTML = "<span>ID: </span>"+state.data.drinks[i].idDrink+" ";
        document.getElementById(i).append(printIdDrink);
  
        var printStrDrink = document.createElement("p"); 
        printStrDrink.setAttribute("id","StrDrink");
        printStrDrink.setAttribute("class","StrDrink");
        printStrDrink.innerHTML = "<span>Drink Name: </span>"+state.data.drinks[i].strDrink+" ";
        document.getElementById(i).append(printStrDrink);
  
        var printStrDrinkThumb = document.createElement("img"); 
        printStrDrinkThumb.className="DrinkThumb";
        printStrDrinkThumb.src= state.data.drinks[i].strDrinkThumb;
        document.getElementById(i).append(printStrDrinkThumb);
 
     
    } }
    /*
    function resultByNonAlcohol(state){
    console.log("zior5")
    let result = document.getElementById("PrintResult");
    result.innerHTML = "";
    for (let i = 0; i < state.data.drinks.length; i++) {
    console.log("zior"+i);
    
    
    let printIdDrink = document.createElement("p");
    printIdDrink.className="IdDrink";
    printIdDrink.innerHTML = "<span>ID: </span>"+state.data.drinks[i].idDrink;
    
    let printStrDrink = document.createElement("span");
    printStrDrink.className="StrDrink";
    printStrDrink.innerHTML = "<span>Drink Name: </span>"+state.data.drinks[i].strDrink+"<br/>";
    
    let printStrDrinkThumb = document.createElement("img");
    printStrDrinkThumb.className="DrinkThumb";
    printStrDrinkThumb.src = state.data.drinks[i].strDrinkThumb;
    
    let printSpace = document.createElement("div");
    printSpace.className="DivSpace";
    printSpace.innerHTML = "<p><p/>";
    
    result.append(printIdDrink);
    result.append(printStrDrink);
    result.append(printStrDrinkThumb);
    
    result.append(printSpace);
    
    
    } }*/

//Megatestsuperchupiguaychachipistachi

console.log("Uno dos unos dos, probandoooo");
function devuelve1() {
  return 1;
}
try {
  module.exports = {
    devuelve1
  };
} catch {}